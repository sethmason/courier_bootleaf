var map, featureList, segmentSearch = [], wqSearch = [], gaugeSearch = [], structureSearch = []; 

//populate data request form with user selected file types
function checkit(value){
    var wqMimeType = $("#wq_file_type").val();
    $("#wqMimeType").attr("value", wqMimeType);
    
    var siteMimeType = $("#site_file_type").val();
    $("#siteMimeType").attr("value", siteMimeType);
    
    var gaugeDataFormat = $("#gaugeData_file_type").val();
    $("#gaugeDataFormat").attr("value", gaugeDataFormat);
    
    var gaugeSiteFormat = $("#gaugeSite_file_type").val();
    $("#gaugeSiteFormat").attr("value", gaugeSiteFormat);
};

//apply the appropirate format to dates for data request forms
$(function() {
    $( "#startDatePicker" ).datepicker({
        altField: "#startDateLo",
        altFormat: "mm-dd-yy",
        dateFormat: 'mm-dd-yy',
    });
});

$(function() {
    $( "#endDatePicker" ).datepicker({
        altField: "#startDateHi",
        altFormat: "mm-dd-yy",
        dateFormat: 'mm-dd-yy',
    });
});

$(function() {
    $( "#gaugeStartDatePicker" ).datepicker({
        altField: "#startDT",
        altFormat: "yy-mm-dd",
        dateFormat: 'yy-mm-dd',
    });
});

$(function() {
    $( "#gaugeEndDatePicker" ).datepicker({
        altField: "#endDT",
        altFormat: "yy-mm-dd",
        dateFormat: 'yy-mm-dd',
    });
});


$(window).resize(function() {
  sizeLayerControl();
});

$(document).on("click", ".feature-row", function(e) {
  $(document).off("mouseout", ".feature-row", clearHighlight);
  sidebarClick(parseInt($(this).attr("id"), 10));
});

$(document).on("mouseover", ".feature-row", function(e) {
  highlight.clearLayers().addLayer(L.circleMarker([$(this).attr("lat"), $(this).attr("lng")], highlightStyle));
});

$(document).on("mouseout", ".feature-row", clearHighlight);

$("#about-btn").click(function() {
  $("#aboutModal").modal("show");
  $(".navbar-collapse.in").collapse("hide");
  return false;
});

$("#full-extent-btn").click(function() {
  map.fitBounds(watershed.getBounds());
  $(".navbar-collapse.in").collapse("hide");
  return false;
});

$("#legend-btn").click(function() {
  $("#legendModal").modal("show");
  $(".navbar-collapse.in").collapse("hide");
  return false;
});

$("#login-btn").click(function() {
  $("#loginModal").modal("show");
  $(".navbar-collapse.in").collapse("hide");
  return false;
});

$("#list-btn").click(function() {
  $('#sidebar').toggle();
  map.invalidateSize();
  return false;
});

$("#nav-btn").click(function() {
  $(".navbar-collapse").collapse("toggle");
  return false;
});

$("#sidebar-toggle-btn").click(function() {
  $("#sidebar").toggle();
  map.invalidateSize();
  return false;
});

$("#sidebar-hide-btn").click(function() {
  $('#sidebar').hide();
  map.invalidateSize();
});

function responseHandler(res) {
    var flatArray = [];
    $.each(res, function(i, element) { 
        flatArray.push(JSON.flatten(element));
    });
    return flatArray;
}; 

function makeRequest(submit_url) {
  $.ajax({
    type: "POST",
    url: submit_url,
    success: function(data) { alert("ajax worked"); },
    error: function(data) {alert("ajax error"); },
  });
}

function sizeLayerControl() {
  $(".leaflet-control-layers").css("max-height", $("#map").height() - 50);
}

//return an array of keys that match on a certain value
function getKeys(obj, val) {
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(getKeys(obj[i], val));
        } else if (obj[i] == val) {
            objects.push(i);
        }
    }
    return objects;
}

function clearHighlight() {
  highlight.clearLayers();
}

function sidebarClick(id) {
  var layer = markerClusters.getLayer(id);
  map.setView([layer.getLatLng().lat, layer.getLatLng().lng], 17);
  layer.fire("click");
  /* Hide sidebar and go to the map on small screens */
  if (document.body.clientWidth <= 767) {
    $("#sidebar").hide();
    map.invalidateSize();
  }
}

function syncSidebar() {
  /* Empty sidebar features */
  $("#feature-list tbody").empty();
  /* Loop through wq_sites layer and add only features which are in the map bounds */
  wq_sites.eachLayer(function (layer) {
    if (map.hasLayer(wq_siteLayer)) {
      if (map.getBounds().contains(layer.getLatLng())) {
        $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="22" height="22" src="assets/img/beaker.png"></td><td class="feature-description">' + layer.feature.properties.MonitoringLocationName + '<p style="font-size: 10px" >(' + layer.feature.properties.MonitoringLocationIdentifier + ')</p></td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      }
    }
  });
  /* Loop through gauge_sites layer and add only features which are in the map bounds */
  gauge_sites.eachLayer(function (layer) {
    if (map.hasLayer(gaugeLayer)) {
      if (map.getBounds().contains(layer.getLatLng())) {
        $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="22" height="22" src="assets/img/droplet.png"></td><td class="feature-description">' + layer.feature.properties.Description + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      }
    }
  });
  /* Loop through dwr_structures layer and add only features which are in the map bounds */
  dwr_structures.eachLayer(function (layer) {
    if (map.hasLayer(structureLayer)) {
      if (map.getBounds().contains(layer.getLatLng())) {
        $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="22" height="22" src="assets/img/faucet.png"></td><td class="feature-description">' + layer.feature.properties.structname + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      }
    }
  });
  /* Update list.js featureList */
  featureList = new List("features", {
    valueNames: ["feature-description"]
  });
  featureList.sort("feature-description", {
    order: "asc"
  });
}

/* Basemap Layers */
var mapQuest = MQ.mapLayer(),
    map;


/* Overlay Layers */
var highlight = L.geoJson(null);
var highlightStyle = {
  stroke: true,
  weight: 3.0,
  color: "red",
  fillOpacity: 0.0,
  radius: 14
};

var watershed = L.geoJson(null, {
  style: function (feature) {
    return {
      color: "black",
      fill: false,
      opacity: 1,
      clickable: false
    };
  },
  onEachFeature: function (feature, layer) {
    segmentSearch.push({
      description: layer.feature.properties.segmentName,
      source: "Watershed Bounds",
      id: L.stamp(layer),
      bounds: layer.getBounds()
    });
  }
});
$.getJSON("data/watershed.geojson", function (data) {
  watershed.addData(data);
});

var HUC10 = L.geoJson(null, {
  style: function (feature) {
    return {
      color: feature.properties.Color,
      fill: true,
      opacity: 0.5,
      clickable: true
    };
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
        var content = "<table class='table table-striped table-bordered table-condensed'>" +
          "<tr><th>HUC 10</th><td>" + feature.properties.HUC10 + "</td></tr>" +
          "<tr><th>Area (acres)</th><td>" + feature.properties.AreaAcres + "</td></tr>" +
          "<table>"+
          "<h4>Additional Resources</h4>" +
          "<p><a href='data/WQ_Reports/" +feature.properties.WQ_Report + "' target='_blank'>ERWC Water Quality Report</a></p>";
        layer.on({
          click: function (e) {
            $("#feature-title").html(feature.properties.Name + " Watershed");
            $("#feature-info").html(content);
            $("#featureModal").modal("show");
            highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], highlightStyle));
          }
        });
        //segmentSearch.push({
        //    description: layer.feature.properties.Name,
        //    source: "HUC10 Bounds",
        //    id: L.stamp(layer),
        //    bounds: layer.getBounds()
        //});
    }
  }
});
$.getJSON("data/HUC10.geojson", function (data) {
  HUC10.addData(data);
});

var superfund = L.geoJson(null, {
  style: function (feature) {
    return {
      color: "red",
      weight: 1.0,
      fill: true,
      opacity: 0.75,
      clickable: true
    };
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
        var content = "<h4>Site Description</h4>" +
          "<p>The Eagle Mine Superfund site is located in Eagle County, Colorado, approximately " +
          "one mile southeast of Minturn. The site is defined as the area impacted by past " +
          "mining activity along the Eagle River between the towns of Red Cliff and Minturn. " +
          "Miners began working the Eagle Mine in the 1880s, searching for gold and silver. " +
          "The Eagle Mine later became a large zinc mining operation, leaving high levels of " +
          "arsenic, cadmium, copper, lead and zinc in the soil and in surface and groundwater. " +
          "Copper-silver production continued at Eagle Mine until the mine workings were allowed " +
          "to flood and the mine closed in 1984." +
          "</p>" +
          "<p>EPA placed the Eagle Mine site on its National Priorities List (NPL) in 1986. " +
          "The Colorado Department of Public Health and the Environment (CDPHE) and the potentially " +
          "responsible party, Viacom (today, CBS Operations, Inc.), initiated a cleanup plan in 1988. " +
          "In 1989, EPA became more involved in the project, resulting in additional cleanup measures. " +
          "<h4>Environmental Concerns</h4>" +
          "<p>Lead, zinc, cadmium, arsenic and manganese associated with the mining wastes. " + 
          "The major pathways of concern are surface water contamination to the Eagle River, " +
          "groundwater contamination and ingestion/inhalation of mining wastes." +
          "</p>" +
          "<h4>Additional Resources</h4>" +
          "<p><a href='http://www2.epa.gov/region8/eagle-mine' target='_blank'>EPA Superfund Information</a></p>" +
          "<p><a href='https://www.colorado.gov/pacific/cdphe/eagle-mine' target='_blank'>CDPHE Site Information</a></p>";
        layer.on({
          click: function (e) {
            $("#feature-title").html(feature.properties.CERCLA_ID);
            $("#feature-info").html(content);
            $("#featureModal").modal("show");
            highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], highlightStyle));
          }
        });
        //segmentSearch.push({
        //    description: layer.feature.properties.Name,
        //    source: "Superfund Bounds",
        //    id: L.stamp(layer),
        //    bounds: layer.getBounds()
        //});
    }
  }
});
$.getJSON("data/superfundBounds.geojson", function (data) {
  superfund.addData(data);
});

var irrigated_parcels = L.geoJson(null, {
  style: function (feature) {
    return {
      color: "green",
      weight: 1.0,
      fill: true,
      opacity: 0.75,
      clickable: true
    };
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
        var content = "<table class='table table-striped table-bordered table-condensed'>" +
          "<tr><th>Parcel ID</th><td>" + feature.properties.PARCEL_ID + "</td></tr>" +
          "<tr><th>Crop Type</th><td>" + feature.properties.CROP_TYPE + "</td></tr>" +
          "<tr><th>Irrigated Area (acres)</th><td>" + feature.properties.ACRES + "</td></tr>" +
          "<tr><th>Irrigation Type</th><td>" + feature.properties.IRRIG_TYPE + "</td></tr>" +
          "<tr><th>Linked DWR Structure ID</th><td>" + feature.properties.SW_WDID1 + "</td></tr>" +
          "<table>";
        layer.on({
          click: function (e) {
            $("#feature-title").html(feature.properties.PARCEL_ID);
            $("#feature-info").html(content);
            $("#featureModal").modal("show");
            highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], highlightStyle));
          }
        });
        //segmentSearch.push({
        //    description: layer.feature.properties.Name,
        //    source: "Irrigated Parcels",
        //    id: L.stamp(layer),
        //    bounds: layer.getBounds()
        //});
    }
  }
});
$.getJSON("data/irrigatedParcels2010.geojson", function (data) {
  irrigated_parcels.addData(data);
});

var ISF_reaches = L.geoJson(null, {
  style: function (feature) {
    return {
      color: "purple",
      weight: 2.0,
      fill: false,
      opacity: 0.75,
      clickable: true
    };
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
        var content = "<table class='table table-striped table-bordered table-condensed'>" +
          "<tr><th>Water Body</th><td>" + feature.properties.Name + "</td></tr>" +
          "<tr><th>ISF ID</th><td>" + feature.properties.ID + "</td></tr>" +
          "<tr><th>Reach Length (miles)</th><td>" + feature.properties.MILES + "</td></tr>" +
          "<tr><th>ISF Type</th><td>" + feature.properties.ISF_Type + "</td></tr>" +
          "<tr><th>Status</th><td>" + feature.properties.Status + "</td></tr>" +
          "<table>";
        layer.on({
          click: function (e) {
            $("#feature-title").html(feature.properties.Name);
            $("#feature-info").html(content);
            $("#featureModal").modal("show");
            highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], highlightStyle));
          }
        });
        segmentSearch.push({
            description: layer.feature.properties.Name,
            source: "ISF Reaches",
            id: L.stamp(layer),
            bounds: layer.getBounds()
        });
    }
  }
});
$.getJSON("data/ISF_reaches.geojson", function (data) {
  ISF_reaches.addData(data);
});

var listed_303d = L.geoJson(null, {
  style: function (feature) {
    return {
      color: "red",
      fill: false,
      opacity: 0.8,
      clickable: true
    };
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
        var content = "<table class='table table-striped table-bordered table-condensed'>" +
          "<tr><th>Segment ID</th><td>" + feature.properties.ShortCode + "</td></tr>" +
          "<tr><th>Listing Cycle</th><td>" + feature.properties.CYCLE + "</td></tr>" +
          "<tr><th>303(d) List Parameters</th><td>" + feature.properties.Causes + "</td></tr>" +
          "<tr><th>EPA Waterbody Report</th><td><a class='url-break' href='http://iaspub.epa.gov/tmdl_waters10/attains_waterbody.control?p_list_id=" + feature.properties.AR_2012 +"' target='_blank'>Link</a></td></tr>" +
          "<table>";
        layer.on({
          click: function (e) {
            $("#feature-title").html(feature.properties.ShortCode);
            $("#feature-info").html(content);
            $("#featureModal").modal("show");
            highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], highlightStyle));
          }
        });
        segmentSearch.push({
          description: layer.feature.properties.segmentName,
          source: "303(d) Listed Segments",
          id: L.stamp(layer),
          bounds: layer.getBounds()
        });
      }
    }
});
$.getJSON("data/303d.geojson", function (data) {
  listed_303d.addData(data);
});

var listed_ME = L.geoJson(null, {
  style: function (feature) {
    return {
      color: "yellow",
      fill: false,
      opacity: 0.8,
      clickable: true
    };
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
        var listing = getKeys(feature, 'Y')
        var content = "<table class='table table-striped table-bordered table-condensed'>" +
          "<tr><th>Segment ID</th><td>" + feature.properties.ShortCode + "</td></tr>" +
          "<tr><th>Listing Cycle</th><td>" + feature.properties.CYCLE + "</td></tr>" +
          "<tr><th>Monitoring and Evaluation Parameters</th><td>" + String(listing) + "</td></tr>" +
          "<table>";
        layer.on({
          click: function (e) {
            $("#feature-title").html(feature.properties.ShortCode);
            $("#feature-info").html(content);
            $("#featureModal").modal("show");
            highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], highlightStyle));
          }
        });
        segmentSearch.push({
        description: layer.feature.properties.segmentName,
        source: "M&E Listed Segments",
        id: L.stamp(layer),
        bounds: layer.getBounds()
      });
    }
  }
});
$.getJSON("data/M&E.geojson", function (data) {
  listed_ME.addData(data);
});

var wqcd_segments = L.geoJson(null, {
  style: function (feature) {
    return {
      color: "blue",
      weight: 2.0,
      opacity: 0.8,
      clickable: true
    };
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
        var content = "<table class='table table-striped table-bordered table-condensed'>" +
          "<tr><th>Segment ID</th><td>" + feature.properties.Segment + "</td></tr>" +
          "<tr><th>Assessment Unit</th><td>" + feature.properties.AssessUnit + "</td></tr>" +
          "<tr><th>Length (miles)</th><td>" + feature.properties.CALC_MILES + "</td></tr>" +
          "<tr><th>ERWC Assessment</th><td>" + feature.properties.Severity + "</td></tr>" +
          "<table>" +
          "<h5>Additional Resources</h5>" +
          "<p><a class='url-break' href='" + feature.properties.StandLink +"' target='_blank'>Water Quality Standards Tables</a></p>" +
          "<p><a class='url-break' href='data/WQ_Reports/" + feature.properties.WQ_Report +"' target='_blank'>ERWC Water Quality Report</a>" +
          "</p>";
        layer.on({
          click: function (e) {
            $("#feature-title").html(feature.properties.Segment);
            $("#feature-info").html(content);
            $("#featureModal").modal("show");
            highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], highlightStyle));
          }
        });
        segmentSearch.push({
          description: layer.feature.properties.segmentName,
          source: "WQCD Segments",
          id: L.stamp(layer),
          bounds: layer.getBounds()
        });
    }
  }
});
$.getJSON("data/wqcd_segments.geojson", function (data) {
  wqcd_segments.addData(data);
});


/* Single marker cluster layer to hold all clusters */
var markerClusters = new L.MarkerClusterGroup({
  spiderfyOnMaxZoom: true,
  showCoverageOnHover: true,
  zoomToBoundsOnClick: true
});

/* Empty layer placeholder to add to layer control for listening when to add/remove wq_sites to markerClusters layer */
var wq_siteLayer = L.geoJson(null);
var wq_sites = L.geoJson(null, {
  pointToLayer: function (feature, latlng) {
    return L.marker(latlng, {
      icon: L.icon({
        iconUrl: "assets/img/beaker.png",
        iconSize: [22, 22],
        popupAnchor: [0, -25]
      }),
      title: feature.properties.MonitoringLocationName,
      riseOnHover: true
    });
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
        var content = "<table class='table table-striped table-bordered table-condensed'>" +
            "<tr><th>Name</th><td>" + feature.properties.MonitoringLocationName + "</td></tr>" +
            "<tr><th>Site ID</th><td>" + feature.properties.MonitoringLocationIdentifier + "</td></tr>" +
            "<tr><th>Organization</th><td>" + feature.properties.OrganizationFormalName + "</td></tr>" +
            "<tr><th>Type</th><td>" + feature.properties.MonitoringLocationTypeName + "</td></tr>" +
            "<table>";
      layer.on({
        click: function (e) {
          $.get("http://lotichydrological.com/iwqm-cake/stations_variables/by_name/" + feature.properties.MonitoringLocationIdentifier + ".json", function(data){
            var summary = data.data.Variable;
            $('#wq-summary').bootstrapTable({
                data: summary,
                responseHandler: responseHandler
            });
            $('#wq-summary').bootstrapTable('load', summary);
          });
          $("#wq-title").html(feature.properties.MonitoringLocationName);
          $("#wq-info").html(content);
          $("#wq-download").attr("formaction", "http://waterqualitydata.us/Result/search?")
          $("#site-download").attr("formaction", "http://waterqualitydata.us/Station/search?")
          $("#wq-form-data").attr("value", feature.properties.MonitoringLocationIdentifier)
          $("#site-form-data").attr("value", feature.properties.MonitoringLocationIdentifier)
          $("#wqModal").modal("show");
          highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], highlightStyle));
        }
      });
      $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/water_quality.png"></td><td class="feature-description">' + layer.feature.properties.MonitoringLocationName + '<p style="font-size: 10px" >(' + layer.feature.properties.MonitoringLocationIdentifier + ')</p></td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      wqSearch.push({
        description: layer.feature.properties.MonitoringLocationName,
        name: layer.feature.properties.MonitoringLocationIdentifier,
        source: "Water Quality Sites",
        id: L.stamp(layer),
        lat: layer.feature.geometry.coordinates[1],
        lng: layer.feature.geometry.coordinates[0]
      });
    }
  }
});
$.getJSON("data/wqStations.geojson", function (data) {
  wq_sites.addData(data);
  map.addLayer(wq_siteLayer);
});

/* Empty layer placeholder to add to layer control for listening when to add/remove gauge_sites to markerClusters layer */
var gaugeLayer = L.geoJson(null);
var gauge_sites = L.geoJson(null, {
  pointToLayer: function (feature, latlng) {
    return L.marker(latlng, {
      icon: L.icon({
        iconUrl: "assets/img/droplet.png",
        iconSize: [22, 22],
        //iconAnchor: [12, 21],
        popupAnchor: [0, -25]
      }),
      title: feature.properties.Description,
      riseOnHover: true
    });
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
      var content = "<table class='table table-striped table-bordered table-condensed'>" +
        "<tr><th>Name</th><td>" + feature.properties.Description + "</td></tr>" +
        "<tr><th>Site ID</th><td>" + feature.properties.Name + "</td></tr>" +
        "<tr><th>Website</th><td><a class='url-break' href='http://waterdata.usgs.gov/nwis/inventory?agency_code=USGS&site_no=" + feature.properties.Name +"' target='_blank'>Link</a></td></tr>" +
        "<table>";
      layer.on({  
        click: function (e) {
          $("#gauge-title").html(feature.properties.Description);
          $("#gauge-info").html(content);
          $("#gauge-graph").attr("src", "http://waterdata.usgs.gov/nwisweb/graph?agency_cd=USGS&site_no=" + feature.properties.Name + "&parm_cd=00060&period=7")
          $("#gaugeData-download").attr("formaction", "http://waterservices.usgs.gov/nwis/iv/?")
          $("#gaugeSite-download").attr("formaction", "http://waterservices.usgs.gov/nwis/site/?")
          $("#gaugeData-form-data").attr("value", feature.properties.Name)
          $("#gaugeSite-form-data").attr("value", feature.properties.Name)
          $("#gaugeModal").modal("show");
          highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], highlightStyle));
        }
      });
      $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/streamgauge_marker.png"></td><td class="feature-description">' + layer.feature.properties.Description + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      gaugeSearch.push({
        description: layer.feature.properties.Description,
        name: layer.feature.properties.Name,
        source: "Stream Gauges",
        id: L.stamp(layer),
        lat: layer.feature.geometry.coordinates[1],
        lng: layer.feature.geometry.coordinates[0]
      });
    }
  }
});
$.getJSON("data/realtimeSites.geojson", function (data) {
  gauge_sites.addData(data);
});

var structureLayer = L.geoJson(null);
var dwr_structures = L.geoJson(null, {
    pointToLayer: function (feature, latlng) {
    return L.marker(latlng, {
      icon: L.icon({
        iconUrl: "assets/img/faucet.png",
        iconSize: [22, 22],
        popupAnchor: [0, -25]
      }),
      title: feature.properties.structname,
      riseOnHover: true
    });
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
      var content = "<table class='table table-striped table-bordered table-condensed'>" +
        "<tr><th>Name</th><td>" + feature.properties.structname + "</td></tr>" +
        "<tr><th>WDID</th><td>" + feature.properties.wdid + "</td></tr>" +
        "<tr><th>Type</th><td>" + feature.properties.structtype + "</td></tr>" +
        "<tr><th>Feature Type</th><td>" + feature.properties.featuretyp + "</td></tr>" +
        "<tr><th>Water Source</th><td>" + feature.properties.watersrc + "</td></tr>" +
        "<tr><th>Absolute Decreed Rate (cfs)</th><td>" + feature.properties.dcrratabs + "</td></tr>" +
        "<tr><th>Conditional Decreed Rate (cfs)</th><td>" + feature.properties.dcrratcond + "</td></tr>" +
        "<tr><th>Absolute Decreed Volume (acre feet)</th><td>" + feature.properties.dcrvolabs + "</td></tr>" +
        "<tr><th>Conditional Decreed Volume (acre feet)</th><td>" + feature.properties.dcrvolcond + "</td></tr>" +
        "<tr><th>Status</th><td>" + feature.properties.currinuse + "</td></tr>" +
        "<tr><th>More Info</th><td><a class='url-break' href='" + feature.properties.moreinfo +"' target='_blank'>Link</a></td></tr>" +
        "<table>";
      layer.on({
        click: function (e) {
          $("#feature-title").html(feature.properties.structname);
          $("#feature-info").html(content);
          $("#featureModal").modal("show");
          highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], highlightStyle));
        }
      });
      $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/well.png"></td><td class="feature-description">' + layer.feature.properties.structname + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      structureSearch.push({
        description: layer.feature.properties.structname,
        name: layer.feature.properties.wdid,
        source: "DWR Structures",
        id: L.stamp(layer),
        lat: layer.feature.geometry.coordinates[1],
        lng: layer.feature.geometry.coordinates[0]
      });
    }
  }
});
$.getJSON("data/DWR_structures.geojson", function (data) {
  dwr_structures.addData(data);
  //map.addLayer(structureLayer);
});

map = L.map("map", {
  zoom: 10,
  center: [39.6, -106.5],
  layers: [mapQuest, watershed, markerClusters, highlight],
  zoomControl: false,
  attributionControl: false
});

/* Layer control listeners that allow for a single markerClusters layer */
map.on("overlayadd", function(e) {
  if (e.layer === wq_siteLayer) {
    markerClusters.addLayer(wq_sites);
    syncSidebar();
  }
  if (e.layer === gaugeLayer) {
    markerClusters.addLayer(gauge_sites);
    syncSidebar();
  }
  if (e.layer === structureLayer) {
    markerClusters.addLayer(dwr_structures);
    syncSidebar();
  }
});

map.on("overlayremove", function(e) {
  if (e.layer === wq_siteLayer) {
    markerClusters.removeLayer(wq_sites);
    syncSidebar();
  }
  if (e.layer === gaugeLayer) {
    markerClusters.removeLayer(gauge_sites);
    syncSidebar();
  }
  if (e.layer === structureLayer) {
    markerClusters.removeLayer(dwr_structures);
    syncSidebar();
  }
});

/* Filter sidebar feature list to only show features in current map bounds */
map.on("moveend", function (e) {
  syncSidebar();
});

/* Clear feature highlight when map is clicked */
map.on("click", function(e) {
  highlight.clearLayers();
});

/* Attribution control */
function updateAttribution(e) {
  $.each(map._layers, function(index, layer) {
    if (layer.getAttribution) {
      $("#attribution").html((layer.getAttribution()));
    }
  });
}
map.on("layeradd", updateAttribution);
map.on("layerremove", updateAttribution);

var attributionControl = L.control({
  position: "bottomright"
});
attributionControl.onAdd = function (map) {
  var div = L.DomUtil.create("div", "leaflet-control-attribution");
  div.innerHTML = "<span class='hidden-xs'>Developed by <a href='http://lotichydrological.com'>lotichydrological.com</a> | </span><a href='#' onclick='$(\"#attributionModal\").modal(\"show\"); return false;'>Attribution</a>";
  return div;
};
map.addControl(attributionControl);

var zoomControl = L.control.zoom({
  position: "bottomright"
}).addTo(map);

/* GPS enabled geolocation control set to follow the user's location */
var locateControl = L.control.locate({
  position: "bottomright",
  drawCircle: true,
  follow: true,
  setView: true,
  keepCurrentZoomLevel: true,
  markerStyle: {
    weight: 1,
    opacity: 0.8,
    fillOpacity: 0.8
  },
  circleStyle: {
    weight: 1,
    clickable: false
  },
  icon: "fa fa-location-arrow",
  metric: false,
  strings: {
    title: "My location",
    popup: "You are within {distance} {unit} from this point",
    outsideMapBoundsMsg: "You seem located outside the boundaries of the map"
  },
  locateOptions: {
    maxZoom: 18,
    watch: true,
    enableHighAccuracy: true,
    maximumAge: 10000,
    timeout: 10000
  }
}).addTo(map);

/* Larger screens get expanded layer control and visible sidebar */
if (document.body.clientWidth <= 767) {
  var isCollapsed = true;
} else {
  var isCollapsed = false;
}

var baseLayers = {
  "Street Map": mapQuest,
  "Aerial Imagery": MQ.satelliteLayer(),
  "Hybrid": MQ.hybridLayer(),
  "Grayscale": MQ.lightLayer(),
};

var groupedOverlays = {
  "Data Collection Locations": {
    "<img src='assets/img/beaker.png' width='22' height='22'>&nbsp;Water Quality Sites": wq_siteLayer,
    "<img src='assets/img/droplet.png' width='22' height='22'>&nbsp;Stream Gauges": gaugeLayer
  },
  "Reference Layers": {
    "Eagle Watershed Bounds": watershed,
    "Subwatershed Bounds" : HUC10,
    "Superfund Sites": superfund
  },
  "Water Quality Control Commission": {
    "303(d) Listed Segments": listed_303d,
    "M&E Listed Segments": listed_ME,
    "305(b) Reporting Segments": wqcd_segments
  },
  "Division of Water Resources": {
    "<img src='assets/img/faucet.png' width='22' height='22'>&nbsp; DWR Structures": structureLayer,
    "Irrigated Parcels": irrigated_parcels,
    "ISF Reaches": ISF_reaches
  },
};

var layerControl = L.control.groupedLayers(baseLayers, groupedOverlays, {
  collapsed: isCollapsed
}).addTo(map);

/* Highlight search box text on click */
$("#searchbox").click(function () {
  $(this).select();
});

/* Prevent hitting enter from refreshing the page */
$("#searchbox").keypress(function (e) {
  if (e.which == 13) {
    e.preventDefault();
  }
});

$("#featureModal").on("hidden.bs.modal", function (e) {
  $(document).on("mouseout", ".feature-row", clearHighlight);
});

/* Typeahead search functionality */
$(document).one("ajaxStop", function () {
  $("#loading").hide();
  sizeLayerControl();
  /* Fit map to watershed bounds */
  map.fitBounds(watershed.getBounds());
  featureList = new List("features", {valueNames: ["feature-description"]});
  featureList.sort("feature-description", {order:"asc"});

  //var watershedBH = new Bloodhound({
  //  description: "Watershed Bounds",
  //  datumTokenizer: function (d) {
  //    return Bloodhound.tokenizers.whitespace(d.description);
  //  },
  //  queryTokenizer: Bloodhound.tokenizers.whitespace,
  //  local: segmentSearch,
  //  limit: 10
  //});
  //
  //  var HUC10BH = new Bloodhound({
  //  description: "HUC10 Bounds",
  //  datumTokenizer: function (d) {
  //    return Bloodhound.tokenizers.whitespace(d.description);
  //  },
  //  queryTokenizer: Bloodhound.tokenizers.whitespace,
  //  local: segmentSearch,
  //  limit: 10
  //});

  var wq_sitesBH = new Bloodhound({
    description: "Water Quality Sites",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.description);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: wqSearch,
    limit: 10
  });

  var gauge_sitesBH = new Bloodhound({
    description: "Stream Gauges",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.description);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: gaugeSearch,
    limit: 10
  });
  
  var dwr_structuresBH = new Bloodhound({
    description: "DWR Structures",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.description);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: structureSearch,
    limit: 10
  });

  var geodescriptionsBH = new Bloodhound({
    description: "GeoNames",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.description);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: "http://api.geodescriptions.org/searchJSON?userdescription=bootleaf&featureClass=P&maxRows=5&countryCode=US&description_startsWith=%QUERY",
      filter: function (data) {
        return $.map(data.geodescriptions, function (result) {
          return {
            description: result.description + ", " + result.adminCode1,
            lat: result.lat,
            lng: result.lng,
            source: "GeoNames"
          };
        });
      },
      ajax: {
        beforeSend: function (jqXhr, settings) {
          settings.url += "&east=" + map.getBounds().getEast() + "&west=" + map.getBounds().getWest() + "&north=" + map.getBounds().getNorth() + "&south=" + map.getBounds().getSouth();
          $("#searchicon").removeClass("fa-search").addClass("fa-refresh fa-spin");
        },
        complete: function (jqXHR, status) {
          $('#searchicon').removeClass("fa-refresh fa-spin").addClass("fa-search");
        }
      }
    },
    limit: 10
  });
  //watershedBH.initialize();
  //HUC10BH.initialize();
  wq_sitesBH.initialize();
  gauge_sitesBH.initialize();
  dwr_structuresBH.initialize();
  geodescriptionsBH.initialize();

  /* instantiate the typeahead UI */
  $("#searchbox").typeahead({
    minLength: 3,
    highlight: true,
    hint: false
  }, /*{
    description: "Watershed Bounds",
    displayKey: "description",
    source: watershedBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'>Eagle Watershed Bounds</h4>"
    }
  }, {
    description: "HUC10 Bounds",
    displayKey: "description",
    source: HUC10BH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'>HUC10 Bounds</h4>"
    }
  }, */{
    description: "Water Quality Sites",
    displayKey: "description",
    source: wq_sitesBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'><img src='assets/img/beaker.png' width='25' height='25'>&nbsp;Water Quality Sites</h4>",
      suggestion: Handlebars.compile(["{{description}}<br>&nbsp;<small>{{name}}</small>"].join(""))
    }
  }, {
    description: "Stream Gauges",
    displayKey: "description",
    source: gauge_sitesBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'><img src='assets/img/droplet.png' width='25' height='25'>&nbsp;Stream Gauges</h4>",
      suggestion: Handlebars.compile(["{{description}}<br>&nbsp;<small>{{name}}</small>"].join(""))
    }
  }, {
    description: "DWR Structures",
    displayKey: "description",
    source: dwr_structuresBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'><img src='assets/img/faucet.png' width='25' height='25'>&nbsp;DWR Structures</h4>",
      suggestion: Handlebars.compile(["{{description}}<br>&nbsp;<small>{{name}}</small>"].join(""))
    }
  }, {
    description: "GeoNames",
    displayKey: "description",
    source: geodescriptionsBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'><img src='assets/img/globe.png' width='25' height='25'>&nbsp;GeoNames</h4>"
    }
  }).on("typeahead:selected", function (obj, datum) {
    if (datum.source === "Watershed Bounds") {
      map.fitBounds(datum.bounds);
    }
    if (datum.source === "Water Quality Sites") {
      if (!map.hasLayer(wq_siteLayer)) {
        map.addLayer(wq_siteLayer);
      }
      map.setView([datum.lat, datum.lng], 17);
      if (map._layers[datum.id]) {
        map._layers[datum.id].fire("click");
      }
    }
    if (datum.source === "Stream Gauges") {
      if (!map.hasLayer(gaugeLayer)) {
        map.addLayer(gaugeLayer);
      }
      map.setView([datum.lat, datum.lng], 17);
      if (map._layers[datum.id]) {
        map._layers[datum.id].fire("click");
      }
    }
    if (datum.source === "GeoNames") {
      map.setView([datum.lat, datum.lng], 14);
    }
    if ($(".navbar-collapse").height() > 50) {
      $(".navbar-collapse").collapse("hide");
    }
  }).on("typeahead:opened", function () {
    $(".navbar-collapse.in").css("max-height", $(document).height() - $(".navbar-header").height());
    $(".navbar-collapse.in").css("height", $(document).height() - $(".navbar-header").height());
  }).on("typeahead:closed", function () {
    $(".navbar-collapse.in").css("max-height", "");
    $(".navbar-collapse.in").css("height", "");
  });
  $(".twitter-typeahead").css("position", "static");
  $(".twitter-typeahead").css("display", "block");
});

// Leaflet patch to make layer control scrollable on touch browsers
var container = $(".leaflet-control-layers")[0];
if (!L.Browser.touch) {
  L.DomEvent
  .disableClickPropagation(container)
  .disableScrollPropagation(container);
} else {
  L.DomEvent.disableClickPropagation(container);
}
